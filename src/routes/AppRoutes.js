import React from 'react'
import {Routes, Route } from 'react-router-dom';
import { MoviesPage, MovieDetailsPage, NotFoundPage, SearchPage } from '../pages'

export const AppRoutes = () => {
  return (
    <>
        <Routes>
            <Route path='/' element={ < MoviesPage key='now-playing' apiPath='/now_playing' pageTitle="Now Playing | Film Fiesta" />} />
            <Route path='/movies/:id' element={ < MovieDetailsPage />} />
            <Route path='/top-rated' element={ < MoviesPage key='top-rated' apiPath='/top_rated'
            pageTitle="Top Rated | Film Fiesta" />} />
            <Route path='/popular' element={ < MoviesPage key='popular' apiPath='/popular'
            pageTitle="Popular| Film Fiesta" />} />
            <Route path='/upcoming' element={ < MoviesPage key='upcoming' apiPath='/upcoming'
            pageTitle="Upcoming | Film Fiesta"/>} />
            <Route path='/movies/search' element={ <SearchPage />} />
            <Route path='*' element={ < NotFoundPage />} />
        </Routes>
    </>
    )
}

//can give key to the routes to make it unique, and to make it rerender
