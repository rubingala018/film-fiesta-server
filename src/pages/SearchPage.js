import React, { useEffect } from 'react'
import { useSearchParams } from 'react-router-dom'
import { useDynamicTitle, useFetch } from '../hooks';
import { Alert, MovieCard, Skeleton } from '../components';

export const SearchPage = () => {
    const [params] = useSearchParams();
    const queryString = params.get('q');
    const BASE_URL = process.env.REACT_APP_BASE_URL;
    const API_KEY = process.env.REACT_APP_API_KEY;

    useDynamicTitle(`Results for: ${queryString} | Film Fiesta`);

    const {data, isLoading, error, setUrl} = useFetch();

    useEffect(() => {
        const URL = `${BASE_URL}/3/search/movie?api_key=${API_KEY}&query=${queryString}`;
        setUrl(URL);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[queryString]);

    const renderSkeleton = (nums = 3) => {
        const skeletons = [];
        for (let i=0; i<nums; i++){
          skeletons.push((<Skeleton key={i}/>));
        }
        return skeletons;
      }

  return (
    <main>
        <h1 className='text-3xl dark:text-white text-slate-800'>
            { data && data.results.length === 0 ? `No movies found for ${queryString}`: `Search Results for : ${queryString}`}
        </h1>
        <div className="flex justify-start flex-wrap">
            { isLoading && renderSkeleton(6) }
            { error && <Alert />}
            { data && data.results.map(movie => <MovieCard key={movie.id} movie={movie} /> ) }
        </div>
    </main>
    )
}
