import React, { useEffect } from "react";
import { Alert, MovieCard, Skeleton } from "../components";
import { useDynamicTitle, useFetch } from '../hooks/';

export const MoviesPage = ({ apiPath, pageTitle }) => {
  const { data, isLoading, error, setUrl } = useFetch();
  const API_KEY = process.env.REACT_APP_API_KEY;
  const BASE_URL = process.env.REACT_APP_BASE_URL; 
  
  useDynamicTitle(pageTitle, 'Film Fiesta');

  useEffect (() => {
    const URL = `${BASE_URL}/3/movie${ apiPath }?api_key=${API_KEY}`;
    setUrl(URL);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [apiPath]);

  const renderSkeleton = (nums = 3) => {
    let skeletons = [];
    for (let i=0; i<nums; i++){
      skeletons.push((<Skeleton key={i} />));
    }
    return skeletons;
  }

  return (
    <main>
      <div className="flex justify-start flex-wrap">
        { isLoading && renderSkeleton(3)}
        { error && <Alert /> }
        { data && data.results && data.results.map(movie => (<MovieCard key={movie.id} movie= { movie } />))}
      </div>
    </main>
  );
};



