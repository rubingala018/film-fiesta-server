export { MoviesPage } from './MoviesPage';
export { MovieDetailsPage } from './MovieDetailsPage';
export { NotFoundPage } from './NotFoundPage';
export { SearchPage} from './SearchPage';