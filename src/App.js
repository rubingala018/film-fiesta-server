import { Header, Footer, ScrollToTop } from "./components/";
import { AppRoutes } from "./routes/AppRoutes";


function App() {
  return (
    <div className="dark:bg-slate-800">
    <Header />
    <ScrollToTop />
    <AppRoutes />
    <Footer />
    </div>
  );
}

export default App;
